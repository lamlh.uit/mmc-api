# MMC-API

## Library Dependencies
- Router: Gin-Gonic
- Database: 
    - ORM: Gorm
    - Migration: Goose (Pressly version)

## Commands
### Database Migrations
- Installation:
`go install github.com/pressly/goose/v3/cmd/goose@latest`
- Create:
```bash
goose -dir migrations create migration_name sql
```

- Up:

```bash
goose -dir migrations mysql "user:password@/dbname" up
```

Eg: Up dev

```bash
goose -dir migrations mysql "gkitchen_dev:zEdmmhMQHtN4h2pe@tcp(10.0.1.10:3306)/mmc_dev" up
```

- Down

```bash
goose -dir migrations mysql "user:password@/dbname" down
```
- Check status

```bash
goose -dir migrations mysql "user:password@/dbname" status
```