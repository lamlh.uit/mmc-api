-- +goose Up
CREATE TABLE employee (
	employeeID BIGINT(11) auto_increment NOT NULL,
	employeeFullName varchar(255) NOT NULL,
	phoneNumber varchar(30) NOT NULL,
	password varchar(200) NOT NULL,
	`role` varchar(20) DEFAULT "employee" NOT NULL,
	createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
	updatedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
	CONSTRAINT employee_PK PRIMARY KEY (employeeID),
	CONSTRAINT employee_UN UNIQUE KEY (phoneNumber)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_unicode_ci;

-- +goose Down
SELECT 'down SQL query';
