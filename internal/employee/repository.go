package employee

import (
	"context"
	"fmt"
	"mmc-api/internal/entity"

	"gorm.io/gorm"
)

// Repository encapsulates the logic to access albums from the data source.
type Repository interface {
	Get(ctx context.Context, id int) (entity.Employee, error)
}

// repository persists albums in database
type repository struct {
	db *gorm.DB
}

// NewRepository creates a new album repository
func NewRepository(db *gorm.DB) Repository {
	return repository{db}
}

// Get reads the album with the specified ID from the database.
func (r repository) Get(ctx context.Context, id int) (entity.Employee, error) {
	var employee entity.Employee
	fmt.Print("id: ", id)
	err := r.db.Find(&employee, "employeeID = ?", id).Error
	return employee, err
}
