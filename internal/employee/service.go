package employee

import (
	"context"
	"mmc-api/internal/entity"
)

// Service encapsulates usecase logic for Employee.
type Service interface {
	Get(ctx context.Context, id int) (Employee, error)
}

// Employee represents the data about an employee.
type Employee struct {
	entity.Employee
}

type service struct {
	repo Repository
}

func NewService(repo Repository) Service {
	return service{repo}
}

// Get returns the album with the specified the album ID.
func (s service) Get(ctx context.Context, id int) (Employee, error) {
	employee, err := s.repo.Get(ctx, id)
	if err != nil {
		return Employee{}, err
	}
	return Employee{employee}, nil
}
