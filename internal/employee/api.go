package employee

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func LoadRouter(e *gin.Engine, db *gorm.DB) {
	v1 := e.Group("v1/employee")
	employeeRepo := NewRepository(db)

	employeeService := NewService(employeeRepo)
	res := resource{service: employeeService}
	{
		v1.POST("/", nil)
		v1.GET("/:id", res.get)
	}
}

type resource struct {
	service Service
}

func (r resource) get(c *gin.Context) {
	fmt.Println(c.Param("id"))
	ID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		fmt.Println(err)
		// return err
	}
	employee, err := r.service.Get(c.Request.Context(), ID)
	if err != nil {
		fmt.Println(err)
		// return nil
	}

	c.IndentedJSON(http.StatusOK, employee)
}
