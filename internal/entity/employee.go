package entity

type Employee struct {
	EmployeeID       int    `gorm:"column:employeeID" json:"employeeID"`
	EmployeeFullName string `gorm:"column:employeeFullName" json:"employeeFullName"`
	// Password  string
	// Email     string `json:"email"`
	// Phone     string `json:"phone"`
	// Role      string `json:"role"`
	// Status    string `json:"status"`
	// CreatedAt string `json:"created_at"`
	// UpdatedAt string `json:"updated_at"`
}
