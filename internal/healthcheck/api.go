package healthcheck

// // RegisterHandlers registers the handlers that perform healthchecks.
// func RegisterHandlers(r *gin.Router, version string) {
// 	r.To("GET,HEAD", "/healthcheck", healthcheck(version))
// }

// // healthcheck responds to a healthcheck request.
// func healthcheck(version string) gin.Handler {
// 	c.JSON(http.StatusOK, gin.H{
// 		"message": "pong",
// 	})
// 	return func(c *gin.Context) error {
// 		return c.Write("OK " + version)
// 	}
// }
