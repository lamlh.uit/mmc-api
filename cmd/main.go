package cmd

import (
	"context"
	"flag"
	"mmc-api/internal/config"
	"mmc-api/internal/employee"
	"mmc-api/pkg/log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

var flagConfig = flag.String("config", "config/local.yml", "path to the config file")
var Version = "0.0.1"

func Main() {
	flag.Parse()
	logger := log.New().With(context.TODO(), "version", Version)
	// Load application configurations
	cfg, err := config.Load(*flagConfig, logger)
	if err != nil {
		logger.Errorf("failed to load application configuration: %s", err)
		os.Exit(-1)
	}

	// Connect to the database
	db, err := gorm.Open(mysql.Open(cfg.DSN), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
	})
	if err != nil {
		logger.Error(err)
		os.Exit(-1)
	}
	defer func() {
		sqlDB, err := db.DB()
		if err != nil {
			logger.Error(err)
			os.Exit(-1)
		}
		sqlDB.Close()
	}()

	// Init the Gin router
	router := gin.Default()
	// Set trusted proxy to nil
	router.SetTrustedProxies(nil)

	// Set default route
	router.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "Welcome To Mamachoice API")
	})

	// Load routes
	// v1 := router.Group("/employee")

	employee.LoadRouter(router, db)
	// {
	// 	v1.GET("/", employee.Service.Get(context.TODO(), 1))
	// 	v1.POST("/", employee.CreateEmployee)
	// }

	// Run the server with specified port
	router.Run(cfg.ServerPort)
}

func RegisterRoutes(router *gin.Engine) {
	// Register routes
	router.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "Welcome To Mamachoice API")
	})
}
